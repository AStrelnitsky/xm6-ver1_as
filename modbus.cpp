#include "modbus.h"
#include "QDebug"

uint16_t getCheckSumm16b(QByteArray buf, int len);
uint8_t isCheckSumm16bCorrect(char *pcBlock, int len);
uint16_t swapShortBytes(uint16_t bytes);

const uint8_t MB_CRC_Table_Hi[] = {     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
                                        0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
                                        0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
                                        0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
                                        0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81,
                                        0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
                                        0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
                                        0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
                                        0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
                                        0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
                                        0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
                                        0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
                                        0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
                                        0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
                                        0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
                                        0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
                                        0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
                                        0x40    };

const uint8_t MB_CRC_Table_Lo[] = {     0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4,
                                        0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
                                        0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD,
                                        0x1D, 0x1C, 0xDC, 0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
                                        0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7,
                                        0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
                                        0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE,
                                        0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
                                        0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2,
                                        0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
                                        0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB,
                                        0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
                                        0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0, 0x50, 0x90, 0x91,
                                        0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
                                        0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88,
                                        0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
                                        0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80,
                                        0x40    };

Modbus::Modbus()
{
    isRequest       = false;
    crcErrorCount   = 0;
    new_tx = false;
}

ModbusFrame Modbus::parseMessage(QByteArray message)
{
    ModbusFrame frame, frame_response;

    uint16_t checksum_calc = getCheckSumm16b(message,6); //some dirty hack
    QDataStream stream(&message, QIODevice::ReadOnly);

    stream >> frame.slave_id;           // 1 byte
    stream >> frame.function_code;      // 1 byte
    // DATA
    stream >> frame.data.address;       // 2 bytes
    stream >> frame.data.data;          // 2 bytes
    // END OF DATA
    stream >> frame.CRC;                // 2 bytes
    frame.CRC = swapShortBytes(frame.CRC);
    // CRC check
    if(frame.CRC != checksum_calc) {
        ModbusFrame blank;
        crcErrorCount++;
        return blank;
       // return frame;
    }
    if(message.size() > 8)
    {
        message.remove(0,8);
        checksum_calc = getCheckSumm16b(message,(message.size() - 2));
        QDataStream stream1(&message, QIODevice::ReadOnly);
        stream1 >> frame_response.slave_id;           // 1 byte
        qDebug() << "ID" << frame_response.slave_id;
        stream1 >> frame_response.function_code;      // 1 byte
        qDebug() << "CODE" << frame_response.function_code;
        if(!(frame_response.function_code & 0x80))
        {
        // DATA
            stream1 >> frame_response.byte_length;       // 2 bytes
            frame_response.byte_length = swapShortBytes(frame_response.byte_length);
            qDebug() << "LENGTH IS" << frame_response.byte_length;
            for(int i = 0; i < (frame_response.byte_length - 1); i++)
            {
               stream1 >> frame_response.packet_data[i];// = message.at(i + 4);          // 2 bytes
                qDebug() << i << "data is" << frame_response.packet_data[i];
            }
        // END OF DATA
        }
        stream1 >> frame_response.CRC;                // 2 bytes
        frame_response.CRC = swapShortBytes(frame_response.CRC);
        qDebug() << "CRC IS" << checksum_calc;
        qDebug() << "CRC_FRAME_IS" << frame_response.CRC;
        if(frame_response.CRC != checksum_calc) {
            ModbusFrame blank;
            crcErrorCount++;
            return blank;
        }
        switch(frame_response.slave_id)
        {
            case PUMP_ID:
            {
                parse_pump(frame_response);
            }
            break;
            case VALVE_1_ID:
            {
               parse_valve(frame_response);
            }
            break;
            case VALVE_2_ID:
            {
            parse_valve(frame_response);
            }
            break;
            case ROTATOR_1_ID:
            {
            parse_rotator(frame_response);
            }
            break;
            case ROTATOR_2_ID:
            {
            parse_rotator(frame_response);
            }
            break;
            case CONTROL_BOARD_1_ID:
            {
                parse_control_board(frame_response);
            }
            break;
            case CONTROL_BOARD_2_ID:
            {
                parse_control_board(frame_response);
            }
        break;
        default: break;
        }
    }
    // Request/response detection
    if(frame_response.data.address == frame.data.address &&
            frame_response.slave_id == frame.slave_id &&
            frame_response.function_code == frame.function_code) {
        // most likely response
        isRequest = false;
    }
    else {
        // most likely request
        isRequest = true;
    }
    lastFrame = frame_response;//frame;

    return frame_response;//frame;
}

bool Modbus::isCurrentFrameRequest()
{
    return isRequest;
}

unsigned int Modbus::getCrcErrorCount()
{
    return crcErrorCount;
}

uint16_t getCheckSumm16b(QByteArray buf, int len)
{
    uint8_t CRCHi = 0xFF;
    uint8_t CRCLo = 0xFF;
    uint8_t index = 0;
    for(uint8_t i = 0; i < len; i++)
    {
        index = CRCLo ^ buf.data()[i];
        CRCLo = CRCHi ^ MB_CRC_Table_Hi[index];
        CRCHi = MB_CRC_Table_Lo[index];
    }
    return CRCHi << 8 | CRCLo;
}

uint8_t isCheckSumm16bCorrect(char *pcBlock, int len)
{
    uint16_t crc_calculated = getCheckSumm16b(pcBlock, len);

    uint16_t *crc_pointer = reinterpret_cast<uint16_t*>(&pcBlock[len-2]);
    uint16_t crc_got = *crc_pointer;

    if(crc_got == crc_calculated) {
        return true;
    }
    else {
        return false;
    }
}

uint16_t swapShortBytes(uint16_t bytes)
{
    uint8_t* ptr = reinterpret_cast<uint8_t*>(&bytes);
    uint16_t output = 0;
    uint8_t* ptr_out = reinterpret_cast<uint8_t*>(&output);

    ptr_out[0] = ptr[1];
    ptr_out[1] = ptr[0];

    return output;
}
void Modbus::parse_pump(ModbusFrame frame_response)
{
    this->pump_data.pwm = frame_response.packet_data[0] + (uint16_t)(frame_response.packet_data[1] << 8);
    this->pump_data.ramp_up = frame_response.packet_data[2] + (uint16_t)(frame_response.packet_data[3] << 8);
    this->pump_data.current_stop_limit = frame_response.packet_data[4] + (uint16_t)(frame_response.packet_data[5] << 8);
    this->pump_data.current_reduce_limit = frame_response.packet_data[6] + (uint16_t)(frame_response.packet_data[7] << 8);
    this->pump_data.shift_temperature = frame_response.packet_data[8] + (uint16_t)(frame_response.packet_data[9] << 8);
    this->pump_data.ramp_down = frame_response.packet_data[10] + (uint16_t)(frame_response.packet_data[11] << 8);
    this->pump_data.k_n = frame_response.packet_data[12] + (uint16_t)(frame_response.packet_data[13] << 8);
    double secs = (frame_response.packet_data[14] + (uint16_t)(frame_response.packet_data[15] << 8))*1680/84000000;
    this->pump_data.motor_speed = 1/(15*3*secs);
    this->pump_data.motor_temperature = frame_response.packet_data[16] + (uint16_t)(frame_response.packet_data[17] << 8);
    this->pump_data.controller_temperature = frame_response.packet_data[18] + (uint16_t)(frame_response.packet_data[19] << 8);
    this->pump_data.muvdm_fault_counter_1 =  frame_response.packet_data[20] + (uint16_t)(frame_response.packet_data[21] << 8);
    this->pump_data.muvdm_fault_counter_2 =  frame_response.packet_data[22] + (uint16_t)(frame_response.packet_data[23] << 8);
}
void Modbus::parse_valve(ModbusFrame frame_response)
{
    uint16_t id = frame_response.slave_id;
    if((frame_response.data.address == CL0_CH1_CURR) && (frame_response.byte_length == 6))
    {
         this->valve_data[id].cl0_ch_curr[0] = frame_response.packet_data[0] + (uint16_t)(frame_response.packet_data[1] << 8);
         this->valve_data[id].cl0_ch_curr[1] = frame_response.packet_data[2] + (uint16_t)(frame_response.packet_data[3] << 8);//!!!!
         this->valve_data[id].cl0_ch_curr[2] = frame_response.packet_data[4] + (uint16_t)(frame_response.packet_data[5] << 8);
         this->valve_data[id].cl0_ch_curr[3] = frame_response.packet_data[6] + (uint16_t)(frame_response.packet_data[7] << 8);
         this->valve_data[id].input_voltage = frame_response.packet_data[8] + (uint16_t)(frame_response.packet_data[9] << 8);
         this->valve_data[id].input_current = frame_response.packet_data[10] + (uint16_t)(frame_response.packet_data[11] << 8);
    }
    else if((frame_response.data.address == CL1_CH1_CURR) && (frame_response.byte_length == 6))
    {
         this->valve_data[id].cl1_ch_curr[0] = frame_response.packet_data[0] + (uint16_t)(frame_response.packet_data[1] << 8);
         this->valve_data[id].cl1_ch_curr[1] = frame_response.packet_data[2] + (uint16_t)(frame_response.packet_data[3] << 8);
         this->valve_data[id].cl1_ch_curr[2] = frame_response.packet_data[4] + (uint16_t)(frame_response.packet_data[5] << 8);
         this->valve_data[id].cl1_ch_curr[3] = frame_response.packet_data[6] + (uint16_t)(frame_response.packet_data[7] << 8);
         this->valve_data[id].voltage_12V = frame_response.packet_data[8] + (uint16_t)(frame_response.packet_data[9] << 8);
         this->valve_data[id].voltage_5V = frame_response.packet_data[10] + (uint16_t)(frame_response.packet_data[11] << 8);
    }
    else if((frame_response.data.address == CL2_CH1_CURR) && (frame_response.byte_length == 6))
    {
         this->valve_data[id].cl2_ch_curr[0] = frame_response.packet_data[0] + (uint16_t)(frame_response.packet_data[1] << 8);
         this->valve_data[id].cl2_ch_curr[1] = frame_response.packet_data[2] + (uint16_t)(frame_response.packet_data[3] << 8);
         this->valve_data[id].cl2_ch_curr[2] = frame_response.packet_data[4] + (uint16_t)(frame_response.packet_data[5] << 8);
         this->valve_data[id].cl2_ch_curr[3] = frame_response.packet_data[6] + (uint16_t)(frame_response.packet_data[7] << 8);
         this->valve_data[id].cl2_ch5_adc = frame_response.packet_data[8] + (uint16_t)(frame_response.packet_data[9] << 8);
         this->valve_data[id].cl2_ch6_adc = frame_response.packet_data[10] + (uint16_t)(frame_response.packet_data[11] << 8);
    }
    else if((frame_response.data.address == CL3_CH1_CURR) && (frame_response.byte_length == 6))
    {
         this->valve_data[id].cl3_ch_curr[0] = frame_response.packet_data[0] + (uint16_t)(frame_response.packet_data[1] << 8);
         this->valve_data[id].cl3_ch_curr[1] = frame_response.packet_data[2] + (uint16_t)(frame_response.packet_data[3] << 8);
         this->valve_data[id].cl3_ch_curr[2] = frame_response.packet_data[4] + (uint16_t)(frame_response.packet_data[5] << 8);
         this->valve_data[id].cl3_ch_curr[3] = frame_response.packet_data[6] + (uint16_t)(frame_response.packet_data[7] << 8);
         this->valve_data[id].cl3_ch5_adc = frame_response.packet_data[8] + (uint16_t)(frame_response.packet_data[9] << 8);
         this->valve_data[id].cl3_ch6_adc = frame_response.packet_data[10] + (uint16_t)(frame_response.packet_data[11] << 8);
    }
    else if((frame_response.data.address == CL4_CH1_CURR) && (frame_response.byte_length == 6))
    {
         this->valve_data[id].cl4_ch_curr[0] = frame_response.packet_data[0] + (uint16_t)(frame_response.packet_data[1] << 8);
         this->valve_data[id].cl4_ch_curr[1] = frame_response.packet_data[2] + (uint16_t)(frame_response.packet_data[3] << 8);
         this->valve_data[id].cl4_ch_curr[2] = frame_response.packet_data[4] + (uint16_t)(frame_response.packet_data[5] << 8);
         this->valve_data[id].cl4_ch_curr[3] = frame_response.packet_data[6] + (uint16_t)(frame_response.packet_data[7] << 8);
         this->valve_data[id].cl4_ch5_adc = frame_response.packet_data[8] + (uint16_t)(frame_response.packet_data[9] << 8);
         this->valve_data[id].cl4_ch6_adc = frame_response.packet_data[10] + (uint16_t)(frame_response.packet_data[11] << 8);
    }
    else if((frame_response.data.address == CL5_CH1_CURR) && (frame_response.byte_length == 6))
    {
         this->valve_data[id].cl5_ch_curr[0] = frame_response.packet_data[0] + (uint16_t)(frame_response.packet_data[1] << 8);
         this->valve_data[id].cl5_ch_curr[1] = frame_response.packet_data[2] + (uint16_t)(frame_response.packet_data[3] << 8);
         this->valve_data[id].cl5_ch_curr[2] = frame_response.packet_data[4] + (uint16_t)(frame_response.packet_data[5] << 8);
         this->valve_data[id].cl5_ch_curr[3] = frame_response.packet_data[6] + (uint16_t)(frame_response.packet_data[7] << 8);
         this->valve_data[id].cl5_ch5_adc = frame_response.packet_data[8] + (uint16_t)(frame_response.packet_data[9] << 8);
         this->valve_data[id].cl5_ch6_adc = frame_response.packet_data[10] + (uint16_t)(frame_response.packet_data[11] << 8);
    }
    else if((frame_response.data.address == CL6_CH1_CURR) && (frame_response.byte_length == 8))
    {
         this->valve_data[id].cl6_ch_curr[0] = frame_response.packet_data[0] + (uint16_t)(frame_response.packet_data[1] << 8);
         this->valve_data[id].cl6_ch_curr[1] = frame_response.packet_data[2] + (uint16_t)(frame_response.packet_data[3] << 8);
         this->valve_data[id].cl6_ch_curr[2] = frame_response.packet_data[4] + (uint16_t)(frame_response.packet_data[5] << 8);
         this->valve_data[id].cl6_ch_curr[3] = frame_response.packet_data[6] + (uint16_t)(frame_response.packet_data[7] << 8);
         this->valve_data[id].cl6_ch5_adc = frame_response.packet_data[8] + (uint16_t)(frame_response.packet_data[9] << 8);
         this->valve_data[id].cl6_ch6_adc = frame_response.packet_data[10] + (uint16_t)(frame_response.packet_data[11] << 8);
         this->valve_data[id].oil_press = frame_response.packet_data[12] + (uint16_t)(frame_response.packet_data[13] << 8);
         this->valve_data[id].oil_temp = frame_response.packet_data[14] + (uint16_t)(frame_response.packet_data[15] << 8);
    }
    ////FULL RESPONSE////////////////////////
    else if((frame_response.data.address == CL0_CH1_CURR) && (frame_response.byte_length == 44))
    {
         this->valve_data[id].cl0_ch_curr[0] = frame_response.packet_data[0] + (uint16_t)(frame_response.packet_data[1] << 8);
         this->valve_data[id].cl0_ch_curr[1] = frame_response.packet_data[2] + (uint16_t)(frame_response.packet_data[3] << 8);//!!!!
         this->valve_data[id].cl0_ch_curr[2] = frame_response.packet_data[4] + (uint16_t)(frame_response.packet_data[5] << 8);
         this->valve_data[id].cl0_ch_curr[3] = frame_response.packet_data[6] + (uint16_t)(frame_response.packet_data[7] << 8);
         this->valve_data[id].input_voltage = frame_response.packet_data[8] + (uint16_t)(frame_response.packet_data[9] << 8);
         this->valve_data[id].input_current = frame_response.packet_data[10] + (uint16_t)(frame_response.packet_data[11] << 8);

         this->valve_data[id].cl1_ch_curr[0] = frame_response.packet_data[12] + (uint16_t)(frame_response.packet_data[13] << 8);
         this->valve_data[id].cl1_ch_curr[1] = frame_response.packet_data[14] + (uint16_t)(frame_response.packet_data[15] << 8);
         this->valve_data[id].cl1_ch_curr[2] = frame_response.packet_data[16] + (uint16_t)(frame_response.packet_data[17] << 8);
         this->valve_data[id].cl1_ch_curr[3] = frame_response.packet_data[18] + (uint16_t)(frame_response.packet_data[19] << 8);
         this->valve_data[id].voltage_12V = frame_response.packet_data[20] + (uint16_t)(frame_response.packet_data[21] << 8);
         this->valve_data[id].voltage_5V = frame_response.packet_data[22] + (uint16_t)(frame_response.packet_data[23] << 8);

         this->valve_data[id].cl2_ch_curr[0] = frame_response.packet_data[24] + (uint16_t)(frame_response.packet_data[25] << 8);
         this->valve_data[id].cl2_ch_curr[1] = frame_response.packet_data[26] + (uint16_t)(frame_response.packet_data[27] << 8);
         this->valve_data[id].cl2_ch_curr[2] = frame_response.packet_data[28] + (uint16_t)(frame_response.packet_data[29] << 8);
         this->valve_data[id].cl2_ch_curr[3] = frame_response.packet_data[30] + (uint16_t)(frame_response.packet_data[31] << 8);
         this->valve_data[id].cl2_ch5_adc = frame_response.packet_data[32] + (uint16_t)(frame_response.packet_data[33] << 8);
         this->valve_data[id].cl2_ch6_adc = frame_response.packet_data[34] + (uint16_t)(frame_response.packet_data[35] << 8);

         this->valve_data[id].cl3_ch_curr[0] = frame_response.packet_data[36] + (uint16_t)(frame_response.packet_data[37] << 8);
         this->valve_data[id].cl3_ch_curr[1] = frame_response.packet_data[38] + (uint16_t)(frame_response.packet_data[39] << 8);
         this->valve_data[id].cl3_ch_curr[2] = frame_response.packet_data[40] + (uint16_t)(frame_response.packet_data[41] << 8);
         this->valve_data[id].cl3_ch_curr[3] = frame_response.packet_data[42] + (uint16_t)(frame_response.packet_data[43] << 8);
         this->valve_data[id].cl3_ch5_adc = frame_response.packet_data[44] + (uint16_t)(frame_response.packet_data[45] << 8);
         this->valve_data[id].cl3_ch6_adc = frame_response.packet_data[46] + (uint16_t)(frame_response.packet_data[47] << 8);

         this->valve_data[id].cl4_ch_curr[0] = frame_response.packet_data[48] + (uint16_t)(frame_response.packet_data[49] << 8);
         this->valve_data[id].cl4_ch_curr[1] = frame_response.packet_data[50] + (uint16_t)(frame_response.packet_data[51] << 8);
         this->valve_data[id].cl4_ch_curr[2] = frame_response.packet_data[52] + (uint16_t)(frame_response.packet_data[53] << 8);
         this->valve_data[id].cl4_ch_curr[3] = frame_response.packet_data[54] + (uint16_t)(frame_response.packet_data[55] << 8);
         this->valve_data[id].cl4_ch5_adc = frame_response.packet_data[56] + (uint16_t)(frame_response.packet_data[57] << 8);
         this->valve_data[id].cl4_ch6_adc = frame_response.packet_data[58] + (uint16_t)(frame_response.packet_data[59] << 8);

         this->valve_data[id].cl5_ch_curr[0] = frame_response.packet_data[60] + (uint16_t)(frame_response.packet_data[61] << 8);
         this->valve_data[id].cl5_ch_curr[1] = frame_response.packet_data[62] + (uint16_t)(frame_response.packet_data[63] << 8);
         this->valve_data[id].cl5_ch_curr[2] = frame_response.packet_data[64] + (uint16_t)(frame_response.packet_data[65] << 8);
         this->valve_data[id].cl5_ch_curr[3] = frame_response.packet_data[66] + (uint16_t)(frame_response.packet_data[67] << 8);
         this->valve_data[id].cl5_ch5_adc = frame_response.packet_data[68] + (uint16_t)(frame_response.packet_data[69] << 8);
         this->valve_data[id].cl5_ch6_adc = frame_response.packet_data[70] + (uint16_t)(frame_response.packet_data[71] << 8);

         this->valve_data[id].cl6_ch_curr[0] = frame_response.packet_data[72] + (uint16_t)(frame_response.packet_data[73] << 8);
         this->valve_data[id].cl6_ch_curr[1] = frame_response.packet_data[74] + (uint16_t)(frame_response.packet_data[75] << 8);
         this->valve_data[id].cl6_ch_curr[2] = frame_response.packet_data[76] + (uint16_t)(frame_response.packet_data[77] << 8);
         this->valve_data[id].cl6_ch_curr[3] = frame_response.packet_data[78] + (uint16_t)(frame_response.packet_data[79] << 8);
         this->valve_data[id].cl6_ch5_adc = frame_response.packet_data[80] + (uint16_t)(frame_response.packet_data[81] << 8);
         this->valve_data[id].cl6_ch6_adc = frame_response.packet_data[82] + (uint16_t)(frame_response.packet_data[83] << 8);
         this->valve_data[id].oil_press = frame_response.packet_data[84] + (uint16_t)(frame_response.packet_data[85] << 8);
         this->valve_data[id].oil_temp = frame_response.packet_data[86] + (uint16_t)(frame_response.packet_data[87] << 8);
    }
}
void Modbus::parse_rotator(ModbusFrame frame)
{
    if(frame.byte_length <= 2)
    {
        switch(frame.data.address)
        {
            case ROTATOR_SPEED:
            {


            }
        break;
        default: break;
        }
    }
    else if (frame.data.address == ROTATOR_SPEED)
    {
        this->rotator_data->speed = frame.packet_data[0] + (uint16_t)(frame.packet_data[1] << 8);
        this->rotator_data->current_limit = frame.packet_data[2] + (uint16_t)(frame.packet_data[3] << 8);
        this->rotator_data->voltage = frame.packet_data[4] + (uint16_t)(frame.packet_data[5] << 8);
        this->rotator_data->current = frame.packet_data[6] + (uint16_t)(frame.packet_data[7] << 8);
        this->rotator_data->controller_temperature = frame.packet_data[8] + (uint16_t)(frame.packet_data[9] << 8);
        this->rotator_data->motor_temperature = frame.packet_data[10] + (uint16_t)(frame.packet_data[11] << 8);
        this->rotator_data->status = frame.packet_data[12] + (uint16_t)(frame.packet_data[13] << 8);
        this->rotator_data->hall_counter = frame.packet_data[14] + (uint16_t)(frame.packet_data[15] << 8);
        this->rotator_data->n_comm = frame.packet_data[16] + (uint16_t)(frame.packet_data[17] << 8);
        this->rotator_data->rotator_set_pwm = frame.packet_data[18] + (uint16_t)(frame.packet_data[19] << 8);
        this->rotator_data->rotator_work_current_limit = frame.packet_data[20] + (uint16_t)(frame.packet_data[21] << 8);
        this->rotator_data->rotator_connection_timeot = frame.packet_data[22] + (uint16_t)(frame.packet_data[23] << 8);
        this->rotator_data->rotator_stand_still = frame.packet_data[24] + (uint16_t)(frame.packet_data[25] << 8);
        this->rotator_data->rotator_stand_still_timer = frame.packet_data[26] + (uint16_t)(frame.packet_data[27] << 8);
        this->rotator_data->rotator_stand_still_timer = frame.packet_data[28] + (uint16_t)(frame.packet_data[29] << 8);
        this->rotator_data->rotator_stand_still_fault_cler = frame.packet_data[30] + (uint16_t)(frame.packet_data[31] << 8);
        this->rotator_data->controller_temp_protect_on = frame.packet_data[32] + (uint16_t)(frame.packet_data[33] << 8);
        this->rotator_data->controller_temp_protect_off = frame.packet_data[34] + (uint16_t)(frame.packet_data[35] << 8);
        this->rotator_data->motor_temp_protect_on = frame.packet_data[36] + (uint16_t)(frame.packet_data[37] << 8);
        this->rotator_data->motor_temp_protect_off = frame.packet_data[38] + (uint16_t)(frame.packet_data[39] << 8);
    }
}
void Modbus::parse_control_board(ModbusFrame frame_response)
{
    if(frame_response.slave_id == CONTROL_BOARD_1_ID)
    {
        new_tx = true;
    }
}
