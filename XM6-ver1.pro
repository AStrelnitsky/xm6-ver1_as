QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    calibration.cpp \
    compensators.cpp \
    debugwindow.cpp \
    errorwidget.cpp \
    exitbutton.cpp \
    firstrotator.cpp \
    firstvalveblock.cpp \
    leftjoy.cpp \
    main.cpp \
    mainwindow.cpp \
    modbus.cpp \
    pumpingstation.cpp \
    rightjoy.cpp \
    scheme.cpp \
    secondrotator.cpp \
    secondvalveblock.cpp \
    serialclient.cpp

HEADERS += \
    calibration.h \
    compensators.h \
    debugwindow.h \
    errorwidget.h \
    exitbutton.h \
    firstrotator.h \
    firstvalveblock.h \
    leftjoy.h \
    mainwindow.h \
    modbus.h \
    pumpingstation.h \
    rightjoy.h \
    scheme.h \
    secondrotator.h \
    secondvalveblock.h \
    serialclient.h

FORMS += \
    calibration.ui \
    compensators.ui \
    debugwindow.ui \
    errorwidget.ui \
    exitbutton.ui \
    firstrotator.ui \
    firstvalveblock.ui \
    leftjoy.ui \
    mainwindow.ui \
    pumpingstation.ui \
    rightjoy.ui \
    scheme.ui \
    secondrotator.ui \
    secondvalveblock.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc
