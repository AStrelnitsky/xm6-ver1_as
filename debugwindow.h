#ifndef DEBUGWINDOW_H
#define DEBUGWINDOW_H

#include <QWidget>

class LeftJoy;
class RightJoy;
class ExitButton;
class Scheme;

namespace Ui {
class DebugWindow;
}

class DebugWindow : public QWidget
{
    Q_OBJECT

public:
    explicit DebugWindow(QWidget *parent = nullptr);
    ~DebugWindow();

    LeftJoy *leftjoy;
    RightJoy *rightjoy;
    ExitButton *exitbutton;
    Scheme *scheme;



private:
    Ui::DebugWindow *ui;
};

#endif // DEBUGWINDOW_H
