#ifndef SERIALCLIENT_H
#define SERIALCLIENT_H

#include "modbus.h"

#include <QThread>
#include <QSerialPort>
#include <QTimer>

class SerialClient : public QThread
{
    Q_OBJECT

public:
    SerialClient();

    void run();
public slots:

    int exec();

signals:
    void error(QString err);
    void dataUpdated(Modbus * modbus);

private:
    QSerialPort *serialPort;
    QTimer *timeoutTimer;

    bool portConnect(int port);
};

#endif // SERIALCLIENT_H
