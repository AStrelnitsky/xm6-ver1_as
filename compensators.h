#ifndef COMPENSATORS_H
#define COMPENSATORS_H

#include <QWidget>

namespace Ui {
class Compensators;
}

class Compensators : public QWidget
{
    Q_OBJECT

public:
    explicit Compensators(QWidget *parent = nullptr);
    ~Compensators();

private:
    Ui::Compensators *ui;
};

#endif // COMPENSATORS_H
