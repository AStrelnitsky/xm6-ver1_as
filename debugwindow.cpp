#include "debugwindow.h"
#include "ui_debugwindow.h"

#include <QHBoxLayout>
#include <QVBoxLayout>

#include "leftjoy.h"
#include "rightjoy.h"
#include "exitbutton.h"
#include "scheme.h"

DebugWindow::DebugWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DebugWindow)
{
    ui->setupUi(this);
    leftjoy = new LeftJoy (this);
    rightjoy = new RightJoy (this);
    exitbutton = new ExitButton (this);
    scheme = new Scheme (this);

    connect(exitbutton, SIGNAL(hideForm()), this, SLOT(hide()));

    QHBoxLayout *horizont = new QHBoxLayout (this);
    horizont->setMargin(20);
    horizont->setSpacing(20);
    horizont->addWidget(leftjoy);

    QVBoxLayout *vertical = new QVBoxLayout (this);
    vertical->setMargin(20);
    vertical->setSpacing(20);
    vertical->addWidget(scheme);
    vertical->addWidget(exitbutton);

    horizont->addLayout(vertical);
    horizont->addWidget(rightjoy);
}

DebugWindow::~DebugWindow()
{
    delete ui;
}
