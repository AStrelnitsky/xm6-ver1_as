#ifndef PUMPINGSTATION_H
#define PUMPINGSTATION_H

#include <QWidget>
#include "modbus.h"

namespace Ui {
class PumpingStation;
}

class PumpingStation : public QWidget
{
    Q_OBJECT

public:
    explicit PumpingStation(QWidget *parent = nullptr);
    ~PumpingStation();
    PUMP_DATA data;

private:
    Ui::PumpingStation *ui;

public slots:
    void update(ModbusFrame frame);
};

#endif // PUMPINGSTATION_H
