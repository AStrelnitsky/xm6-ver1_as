#ifndef CALIBRATION_H
#define CALIBRATION_H

#include <QWidget>
#include <QPushButton>
#include "debugwindow.h"

namespace Ui {
class Calibration;
}

class Calibration : public QWidget
{
    Q_OBJECT

public:
    explicit Calibration(QWidget *parent = nullptr);
    ~Calibration();

    DebugWindow *secondWindow;

public slots:
    void on_pushButton_clicked();

private:
    Ui::Calibration *ui;
};

#endif // CALIBRATION_H
