#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "QHBoxLayout"
#include "QVBoxLayout"

#include "firstvalveblock.h"
#include "pumpingstation.h"
#include "secondvalveblock.h"
#include "firstrotator.h"
#include "secondrotator.h"
#include "calibration.h"
#include "errorwidget.h"
#include "compensators.h"

#include "serialclient.h"
#include"QDebug"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    firstvalveblock = new FirstValveBlock (this);
    calibration = new Calibration(this);
    pumpingstation = new PumpingStation (this);
    firstrotator = new FirstRotator(this);
    secondrotator = new SecondRotator (this);
    secondvalveblock = new SecondValveBlock (this);
    errorwidget = new ErrorWidget (this);
    compensators = new Compensators(this);

    QHBoxLayout *horizontal = new QHBoxLayout (this);
    horizontal->setSpacing(20);
    horizontal->setMargin(20);
    QVBoxLayout *mainvertical = new QVBoxLayout (this);
    mainvertical->setSpacing(20);
    QHBoxLayout *mainhotizontal = new QHBoxLayout (this);
    mainhotizontal->setSpacing(20);
    QVBoxLayout *leftvalve = new QVBoxLayout(this);
    leftvalve->setSpacing(20);
    leftvalve->addWidget(firstvalveblock);
    leftvalve->addWidget(calibration);
    QHBoxLayout *bottom = new QHBoxLayout (this);
    bottom->setSpacing(20);
    bottom->addWidget(firstrotator);
    bottom->addWidget(secondrotator);
    QVBoxLayout *middle = new QVBoxLayout (this);
    middle->setSpacing(20);
    middle->addWidget(pumpingstation);
    middle->addLayout(bottom);
    mainhotizontal->addLayout(leftvalve);
    mainhotizontal->addLayout(middle);
    mainvertical->addLayout(mainhotizontal);
    mainvertical->addWidget(errorwidget);
    QVBoxLayout *rightvalve = new QVBoxLayout (this);
    rightvalve->setSpacing(20);
    rightvalve->addWidget(secondvalveblock);
    rightvalve->addWidget(compensators);
    horizontal->addLayout(mainvertical);
    horizontal->addLayout(rightvalve);

    SerialClient *serial_client = new SerialClient();
    connect(serial_client, SIGNAL(dataUpdated(Modbus *)), this, SLOT(updateDevices(Modbus *)));
    serial_client->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::updateDevices(Modbus * modbus)
{

    /*
    //qDebug() << "WE ARE IN THE SLOT!" << "SLAVE ID IS" << modbus->lastFrame.slave_id;
    qDebug() << "PUMP CONTROLLER TEMP IS" << modbus->pump_data.controller_temperature;
    qDebug() << "PUMP MOTOR TEMP IS" << modbus->pump_data.motor_temperature;
    qDebug() << "current_reduce_limit is " << modbus->pump_data.current_reduce_limit;
    qDebug() << "current_stop_limit is " << modbus->pump_data.current_stop_limit;
    qDebug() << "hall_T is " << modbus->pump_data.hall_T;
    qDebug() << "k_n is " << modbus->pump_data.k_n;
    qDebug() << "ramp_up is " << modbus->pump_data.ramp_up;
    qDebug() << "ramp_down is " << modbus->pump_data.ramp_down;
    qDebug() << "pwm is " << modbus->pump_data.pwm;
    qDebug() << "muvdm_fault_counter_1 is " << modbus->pump_data.muvdm_fault_counter_1;
    qDebug() << "muvdm_fault_counter_2 is " << modbus->pump_data.muvdm_fault_counter_2;
    */
    qDebug() << "cl0_ch_curr[0]" << modbus->valve_data->cl0_ch_curr[0];
    qDebug() << "cl0_ch_curr[1]" << modbus->valve_data->cl0_ch_curr[1];
    qDebug() << "cl0_ch_curr[2] " << modbus->valve_data->cl0_ch_curr[2];
    qDebug() << "cl0_ch_curr[3]" << modbus->valve_data->cl0_ch_curr[3];

    qDebug() << "cl1_ch_curr[0]" << modbus->valve_data->cl1_ch_curr[0];
    qDebug() << "cl1_ch_curr[1]" << modbus->valve_data->cl1_ch_curr[1];
    qDebug() << "cl1_ch_curr[2] " << modbus->valve_data->cl1_ch_curr[2];
    qDebug() << "cl1_ch_curr[3]" << modbus->valve_data->cl1_ch_curr[3];

    qDebug() << "cl2_ch_curr[0]" << modbus->valve_data->cl2_ch_curr[0];
    qDebug() << "cl2_ch_curr[1]" << modbus->valve_data->cl2_ch_curr[1];
    qDebug() << "cl2_ch_curr[2] " << modbus->valve_data->cl2_ch_curr[2];
    qDebug() << "cl2_ch_curr[3]" << modbus->valve_data->cl2_ch_curr[3];

    qDebug() << "cl3_ch_curr[0]" << modbus->valve_data->cl3_ch_curr[0];
    qDebug() << "cl3_ch_curr[1]" << modbus->valve_data->cl3_ch_curr[1];
    qDebug() << "cl3_ch_curr[2] " << modbus->valve_data->cl3_ch_curr[2];
    qDebug() << "cl3_ch_curr[3]" << modbus->valve_data->cl3_ch_curr[3];

    qDebug() << "cl4_ch_curr[0]" << modbus->valve_data->cl4_ch_curr[0];
    qDebug() << "cl4_ch_curr[1]" << modbus->valve_data->cl4_ch_curr[1];
    qDebug() << "cl4_ch_curr[2] " << modbus->valve_data->cl4_ch_curr[2];
    qDebug() << "cl4_ch_curr[3]" << modbus->valve_data->cl4_ch_curr[3];

    qDebug() << "cl5_ch_curr[0]" << modbus->valve_data->cl5_ch_curr[0];
    qDebug() << "cl5_ch_curr[1]" << modbus->valve_data->cl5_ch_curr[1];
    qDebug() << "cl5_ch_curr[2] " << modbus->valve_data->cl5_ch_curr[2];
    qDebug() << "cl5_ch_curr[3]" << modbus->valve_data->cl5_ch_curr[3];

    qDebug() << "cl6_ch_curr[0]" << modbus->valve_data->cl6_ch_curr[0];
    qDebug() << "cl6_ch_curr[1]" << modbus->valve_data->cl6_ch_curr[1];
    qDebug() << "cl6_ch_curr[2] " << modbus->valve_data->cl6_ch_curr[2];
    qDebug() << "cl6_ch_curr[3]" << modbus->valve_data->cl6_ch_curr[3];

}

