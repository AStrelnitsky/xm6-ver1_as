#ifndef FIRSTVALVEBLOCK_H
#define FIRSTVALVEBLOCK_H

#include <QWidget>

namespace Ui {
class FirstValveBlock;
}

class FirstValveBlock : public QWidget
{
    Q_OBJECT

public:
    explicit FirstValveBlock(QWidget *parent = nullptr);
    ~FirstValveBlock();

private:
    Ui::FirstValveBlock *ui;
};

#endif // FIRSTVALVEBLOCK_H
