#include "scheme.h"
#include "ui_scheme.h"

Scheme::Scheme(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Scheme)
{
    ui->setupUi(this);
}

Scheme::~Scheme()
{
    delete ui;
}
