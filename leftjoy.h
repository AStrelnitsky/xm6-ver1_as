#ifndef LEFTJOY_H
#define LEFTJOY_H

#include <QWidget>

namespace Ui {
class LeftJoy;
}

class LeftJoy : public QWidget
{
    Q_OBJECT

public:
    explicit LeftJoy(QWidget *parent = nullptr);
    ~LeftJoy();

private:
    Ui::LeftJoy *ui;
};

#endif // LEFTJOY_H
