#ifndef SECONDROTATOR_H
#define SECONDROTATOR_H

#include <QWidget>

namespace Ui {
class SecondRotator;
}

class SecondRotator : public QWidget
{
    Q_OBJECT

public:
    explicit SecondRotator(QWidget *parent = nullptr);
    ~SecondRotator();

private:
    Ui::SecondRotator *ui;
};

#endif // SECONDROTATOR_H
