#ifndef FIRSTROTATOR_H
#define FIRSTROTATOR_H

#include <QWidget>

namespace Ui {
class FirstRotator;
}

class FirstRotator : public QWidget
{
    Q_OBJECT

public:
    explicit FirstRotator(QWidget *parent = nullptr);
    ~FirstRotator();

private:
    Ui::FirstRotator *ui;
};

#endif // FIRSTROTATOR_H
