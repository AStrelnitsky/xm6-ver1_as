#include "exitbutton.h"
#include "ui_exitbutton.h"

ExitButton::ExitButton(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ExitButton)
{
    ui->setupUi(this);
}

ExitButton::~ExitButton()
{
    delete ui;
}

void ExitButton::on_pushButton_clicked()
{
    emit hideForm();
}
