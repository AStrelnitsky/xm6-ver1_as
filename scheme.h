#ifndef SCHEME_H
#define SCHEME_H

#include <QWidget>

namespace Ui {
class Scheme;
}

class Scheme : public QWidget
{
    Q_OBJECT

public:
    explicit Scheme(QWidget *parent = nullptr);
    ~Scheme();

private:
    Ui::Scheme *ui;
};

#endif // SCHEME_H
