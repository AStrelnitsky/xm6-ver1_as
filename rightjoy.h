#ifndef RIGHTJOY_H
#define RIGHTJOY_H

#include <QWidget>

namespace Ui {
class RightJoy;
}

class RightJoy : public QWidget
{
    Q_OBJECT

public:
    explicit RightJoy(QWidget *parent = nullptr);
    ~RightJoy();

private:
    Ui::RightJoy *ui;
};

#endif // RIGHTJOY_H
