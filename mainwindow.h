#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include "modbus.h"
#include "serialclient.h"
class FirstValveBlock;
class PumpingStation;
class SecondValveBlock;
class FirstRotator;
class SecondRotator;
class Calibration;
class ErrorWidget;
class Compensators;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    FirstValveBlock *firstvalveblock;
    PumpingStation *pumpingstation;
    SecondValveBlock *secondvalveblock;
    FirstRotator *firstrotator;
    SecondRotator *secondrotator;
    Calibration *calibration;
    ErrorWidget *errorwidget;
    Compensators *compensators;
public slots:
    void updateDevices(Modbus * modbus);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
