#include "calibration.h"
#include "ui_calibration.h"

Calibration::Calibration(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Calibration)
{
    ui->setupUi(this);
    secondWindow = new DebugWindow ();
    connect(this->ui->pushButton, SIGNAL(clicked(bool)),this, SLOT(on_pushButton_clicked()));
}

Calibration::~Calibration()
{
    delete ui;
}

void Calibration::on_pushButton_clicked()
{
    secondWindow->showFullScreen();
}
