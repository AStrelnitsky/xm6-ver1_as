#ifndef EXITBUTTON_H
#define EXITBUTTON_H

#include <QWidget>
#include <mainwindow.h>
#include <debugwindow.h>

namespace Ui {
class ExitButton;
}

class ExitButton : public QWidget
{
    Q_OBJECT

public:
    explicit ExitButton(QWidget *parent = nullptr);
    ~ExitButton();

public slots:
    void on_pushButton_clicked();

signals:
    void hideForm();

private:
    Ui::ExitButton *ui;
};

#endif // EXITBUTTON_H
