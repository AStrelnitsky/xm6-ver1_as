#ifndef MODBUS_H
#define MODBUS_H

#include <QDataStream>
#include <QByteArray>

#define VALVE_1_ID 1
#define VALVE_2_ID 2
#define ROTATOR_1_ID 3
#define ROTATOR_2_ID 4
#define PUMP_ID 5
#define HCU_ID 7
#define CONTROL_BOARD_1_ID 0x12
#define CONTROL_BOARD_2_ID 0x13

#define ROTATOR_SPEED      0
#define ROTATOR_CURRENT_LIMIT 1
#define ROTATOR_VOLTAGE 2
#define ROTATOR_CURRENT 3
#define ROTATOR_CONTROLLER_TEMPERATURE 4
#define ROTATOR_MOTOR_TEMPERATURE 5
#define ROTATOR_STATUS 6
#define ROTATOR_HALL_COUNTER 7
#define ROTATOR_N_COMM 8
#define ROTATOR_SET_PWM 9
#define ROTATOR_WORK_CURRENT_LIMIT 10
#define ROTATOR_CONNECTION_TIMEOUT 11
#define ROTATOR_STAND_STILL_THD 12
#define ROTATOR_STAND_STILL_TIMER 13
#define ROTATOR_STAND_STILL_FAUT_CLEAR 14
#define ROTATOR_TEMP_CONTROLLR_PROTECT_ON 15
#define ROTATOR_TEMP_CONTROLLER_PROTECT_OFF 16
#define ROTATOR_TEMP_MOTOR_PROTECT_ON 17
#define ROTATOR_TEMP_MOTOR_PROTECT_OFF 86
#define ROTATOR_SLAVE_ID 50

#define CL0_CH1_PWM            0
#define CL0_CH2_PWM            1
#define CL0_CH3_PWM            2
#define CL0_CH4_PWM            3

#define CL1_CH1_PWM            4
#define CL1_CH2_PWM            5
#define CL1_CH3_PWM            6
#define CL1_CH4_PWM            7

#define CL2_CH1_PWM            8
#define CL2_CH2_PWM            9
#define CL2_CH3_PWM            10
#define CL2_CH4_PWM            11

#define CL3_CH1_PWM            12
#define CL3_CH2_PWM            13
#define CL3_CH3_PWM            14
#define CL3_CH4_PWM            15

#define CL4_CH1_PWM            16
#define CL4_CH2_PWM            17
#define CL4_CH3_PWM            18
#define CL4_CH4_PWM            19

#define CL5_CH1_PWM            20
#define CL5_CH2_PWM            21
#define CL5_CH3_PWM            22
#define CL5_CH4_PWM            23

#define CL6_CH1_PWM            24
#define CL6_CH2_PWM            25
#define CL6_CH3_PWM            26
#define CL6_CH4_PWM            27

#define CL0_CH1_CURR          28
#define CL0_CH2_CURR          29
#define CL0_CH3_CURR          30
#define CL0_CH4_CURR          31
#define INPUT_VOLTAGE         32
#define INPUT_CURRENT         33


#define CL1_CH1_CURR          34
#define CL1_CH2_CURR          35
#define CL1_CH3_CURR          36
#define CL1_CH4_CURR          37
#define VOLTAGE_12V           38
#define VOLTAGE_5V            39

#define CL2_CH1_CURR          40
#define CL2_CH2_CURR          41
#define CL2_CH3_CURR          42
#define CL2_CH4_CURR          43
#define CL2_CH5_ADC           44
#define CL2_CH6_ADC           45

#define CL3_CH1_CURR          46
#define CL3_CH2_CURR          47
#define CL3_CH3_CURR          48
#define CL3_CH4_CURR          49
#define CL3_CH5_ADC            50
#define CL3_CH6_ADC            51

#define CL4_CH1_CURR          52
#define CL4_CH2_CURR          53
#define CL4_CH3_CURR          54
#define CL4_CH4_CURR          55
#define CL4_CH5_ADC            56
#define CL4_CH6_ADC            57

#define CL5_CH1_CURR          58
#define CL5_CH2_CURR          59
#define CL5_CH3_CURR          60
#define CL5_CH4_CURR          61
#define CL5_CH5_ADC            62
#define CL5_CH6_ADC            63

#define CL6_CH1_CURR          64
#define CL6_CH2_CURR          65
#define CL6_CH3_CURR          66
#define CL6_CH4_CURR          67
#define CL6_CH5_ADC            68
#define CL6_CH6_ADC            69


#define OIL_PRESS             70
#define OIL_TEMP              71

struct PUMP_DATA
{
    uint16_t address;         // 2 bytes
    uint16_t pwm;            // 2 bytes
    float ramp_up;
    float ramp_down;
    uint16_t current_stop_limit;
    uint16_t current_reduce_limit;
    uint16_t k_n;
    uint16_t hall_T;
    uint16_t motor_temperature;
    uint16_t controller_temperature;
    uint16_t muvdm_fault_counter_1;
    uint16_t muvdm_fault_counter_2;
    uint16_t shift_temperature;
    double motor_speed;
};
struct VALVE_DATA
{
    uint16_t cl0_ch_curr[4];
    uint16_t cl1_ch_curr[4];
    uint16_t cl2_ch_curr[4];
    uint16_t cl3_ch_curr[4];
    uint16_t cl4_ch_curr[4];
    uint16_t cl5_ch_curr[4];
    uint16_t cl6_ch_curr[4];

    uint16_t input_voltage;
    uint16_t input_current;

    uint16_t voltage_12V;
    uint16_t voltage_5V;

    uint16_t cl2_ch5_adc;
    uint16_t cl2_ch6_adc;

    uint16_t cl3_ch5_adc;
    uint16_t cl3_ch6_adc;

    uint16_t cl4_ch5_adc;
    uint16_t cl4_ch6_adc;

    uint16_t cl5_ch5_adc;
    uint16_t cl5_ch6_adc;

    uint16_t cl6_ch5_adc;
    uint16_t cl6_ch6_adc;

    uint16_t oil_press;
    uint16_t oil_temp;

};
struct ROTATOR_DATA
{
    uint16_t speed;
    uint16_t current_limit;
    uint16_t voltage;
    uint16_t current;
    uint16_t controller_temperature;
    uint16_t motor_temperature;
    uint16_t status;
    uint16_t hall_counter;
    uint16_t n_comm;
    uint16_t rotator_set_pwm;
    uint16_t rotator_work_current_limit;
    uint16_t rotator_connection_timeot;
    uint16_t rotator_stand_still;
    uint16_t rotator_stand_still_timer;
    uint16_t rotator_stand_still_fault_cler;
    uint16_t controller_temp_protect_on;
    uint16_t controller_temp_protect_off;
    uint16_t motor_temp_protect_on;
    uint16_t motor_temp_protect_off;
    uint16_t rotator_slave_id;

};
struct HCU_DATA
{
    ;
};
struct CONTROL_BOARD_DATA
{

};

struct ModbusDataSingleRegister
{
    uint16_t address;         // 2 bytes
    int16_t data;            // 2 bytes
};

struct ModbusFrame
{
    uint8_t slave_id;                       // 1 byte
    uint8_t function_code;                  // 1 byte
    ModbusDataSingleRegister data;          // 4 bytes
    uint16_t CRC;                           // 2 bytes
    uint16_t byte_length;
    uint8_t packet_data[128];
};

class Modbus
{
public:
    Modbus();

    ModbusFrame parseMessage(QByteArray message);
    bool isCurrentFrameRequest();
    unsigned int getCrcErrorCount();
    ModbusFrame lastFrame;
    PUMP_DATA pump_data;
    VALVE_DATA valve_data[2];
    ROTATOR_DATA rotator_data[2];
    HCU_DATA hcu_data;
    CONTROL_BOARD_DATA control_board_data[2];

    void parse_pump(ModbusFrame frame_response);
    void parse_valve(ModbusFrame frame_response);
    void parse_rotator(ModbusFrame frame_response);
    void parse_control_board(ModbusFrame frame_response);
    bool new_tx;
private:

    bool isRequest;
    unsigned int crcErrorCount;

};

#endif // MODBUS_H
