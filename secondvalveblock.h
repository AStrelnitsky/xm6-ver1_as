#ifndef SECONDVALVEBLOCK_H
#define SECONDVALVEBLOCK_H

#include <QWidget>

namespace Ui {
class SecondValveBlock;
}

class SecondValveBlock : public QWidget
{
    Q_OBJECT

public:
    explicit SecondValveBlock(QWidget *parent = nullptr);
    ~SecondValveBlock();

private:
    Ui::SecondValveBlock *ui;
};

#endif // SECONDVALVEBLOCK_H
