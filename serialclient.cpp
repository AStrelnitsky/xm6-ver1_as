#include "serialclient.h"

#include <QString>
#include <QDebug>

const int MAX_COM_ID = 20;

SerialClient::SerialClient()
{
//    timeoutTimer = new QTimer();
//    connect(timeoutTimer, SIGNAL(timeout()), this, SLOT(timerTick()));


}

bool SerialClient::portConnect(int port)
{
    QString str;
#ifdef unix
    str = "/dev/ttyUSB";
#else
    str = "COM";
#endif

    str.append(QString::number(port));

//    qDebug () << "COM_SERVER: Trying to open port " << str;

    //serialPort = new QSerialPort(str);
    serialPort = new QSerialPort("COM5");
    serialPort->setBaudRate(QSerialPort::BaudRate::Baud115200, QSerialPort::AllDirections);
    serialPort->setDataBits(QSerialPort::DataBits::Data8);
    serialPort->setParity(QSerialPort::Parity::NoParity);
    serialPort->setStopBits(QSerialPort::StopBits::OneStop);
    serialPort->setFlowControl(QSerialPort::FlowControl::NoFlowControl);

    if(serialPort->open(QIODevice::ReadWrite)) {

        qDebug() << "COM_SERVER: port" << str << "successfully opened!";
    }
    else {
        delete serialPort;
        return false;
    }
    return true;
}

void SerialClient::run()
{
    bool opened = false;
//    for(int i=0; i<MAX_COM_ID; i++) {
//        opened = portConnect(i);
//        if(opened) {
//            break;
//        }
//    }
    opened = portConnect(0);

    if(opened) {
        exec();
    }
    else {
        qDebug() << "FAIL PORT OPEN TO (WHEN?) ((WHY?)) (((I DON'T KNOW)))";
    }
}

ModbusFrame parseMessage(QByteArray message);
bool isLastFrameRequest();
unsigned int getCrcErrorCount();

int SerialClient::exec()
{
    Modbus modbus; // lol
    while(1)
    {
        QByteArray msg;
        ModbusFrame frame;
        serialPort->clear();
        serialPort->waitForReadyRead(25);
        long long bytesAvailiable = serialPort->bytesAvailable();
        if (bytesAvailiable)
        {
            msg.clear();
            msg.push_back(serialPort->readAll());


            frame = modbus.parseMessage(msg);

            qDebug() << "MESSAGE:" << msg.toHex()  ;
            qDebug() << "FRAME PARSED (maybe) SLAVE ID: [" << (unsigned char)(frame.slave_id) << "] FUNCTION: [" << (unsigned char)(frame.function_code) << "] ADDRESS: [" << frame.data.address << "] DATA: [" << frame.data.data << "]";
            qDebug() << "CURRENT MESSAGE IS REQUEST: [" << modbus.isCurrentFrameRequest() << "] CRC ERROR COUNT: [" << modbus.getCrcErrorCount() << "]";

             emit dataUpdated(&modbus);
        }
        else {
            //qDebug() << "[SERIAL_CLIENT] Didn't receive answer for message " << messageType;
            qDebug() << "[SERIAL_CLIENT] Bytes available:" << bytesAvailiable;
            serialPort->readAll();
        }
        if(modbus.new_tx)
        {
            modbus.new_tx = false;
            //msg.append()
        }

        //emit dataUpdated(frame);
    }
}
